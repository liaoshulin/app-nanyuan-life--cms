# 《APP设计与原型制作》期末项目---南苑life+APP 
# MRD

### 项目介绍
- 原型：[南苑Life+原型](http://liaoshulin.gitee.io/nanyuan-life-app/#id=bstcid&p=1_1%E5%BC%80%E5%B1%8F%E9%A1%B5&g=1)
- 小组协作式Axure项目
- 中山大学南方学院 文学与传媒学院 网络与新媒体专业

成员：
| 成员  | 学号 |参与MRD文档 | 参与原型制作的贡献 |
|-----|----|------ |----------- |
| 陈舟驰 |  181036108  |产品、需求分析 |1.低保真原型 【圈子】 2.【圈子】文字撰写 3.高保真原型制作 4.用户流程图 |
| 黄智毅 |  181013046  | 市场概述、目标市场| 1.低保真原型(我的【个人版】)2.“我的”部分交互文字撰写3.原型高级交互：系统变量与函数【①获取系统时间②点击页签切换轮播图③虚拟控件四④活动参与人员数量自增减效果】|
|    钟振升 |  181013110  | | 1. 低保真原型【我的-主办方版】2. 产品架构图 3. 中继器交互 4. 完善高保真【我的-主办方版】|
|    黄嘉慧 |   181013060 |用户分析 |1.低保真原型【首页】2.高保真原型制作 3.平静化技术设计原则及制作 4.交互逻辑文字审核 |
|   许泳诗  | 181013070  | 产品理念、竞品分析| 1.CMS产品信息设计- 角色/权限架构图 2. CMS产品信息设计-主办方、用户界面 3.主办方活动流程图 4.平静技术设计原则及制作|
|   廖书琳  | 181013009  | 竞品分析、产品方向| 1、产品经理展示项目 2、gitee仓库管理 3、用户体系架构图 4、CMS产品信息设计-角色/权限架构图 5、CMS产品信息设计-主办方、用户界面 |
|   方乔  | 181013010  |市场细分、市场壁垒 | 1.低保真原型【首页】 2.【首页】交互文字撰写 3.原型高级交互：动态面板嵌套和三个全局变量（①注册账号的跨页面传递 ②活动报名成功的跨页面传递 ③圈子创建成功的跨页面传递）|
# 一、 概述
 ####  **1.1 背景** 

在南苑，学校或社团组织经常会举办一些活动比赛，然而校内信息 平台众多，消息冗杂 ，校内师生想要获取这些信息一般只能够通过各种公众号推文或是口头传播得知，信息的获取渠道十分的 **分散以及碎片化** ，寻求信息需要耗费一定心力，并且难以找到自己感兴趣的活动比赛从而遗憾错过，不能有效实现信息的流通与对接。

目前校内师生、社团组织以及商家在参与/举办活动比赛上存在如下问题：

1. 想要获取活动/比赛的信息需要关注多个公众号，并且 无法及时得知
2. 活动比赛的曝光度不够导致 参与人数不足
3. 提供 综测加分 的活动讲座需要纸质签到签退， 不易保存手续麻烦
4. 难以找到自己感兴趣的活动/比赛 

南苑life+正是针对这些痛点，为南苑师生、社团组织、商家所提供的 **综合性活动信息发布平台** ，汇聚了校内各种精彩的活动信息，丰富校园生活，并且能够针对各类活动进行 **精准推广营销，打破信息壁垒** 。

####  **1.2 目的** 

1. 让南苑学子 快速简易地掌握到校内活动 的一手信息及报名参与
2. 提高 在校内举办的各大活动/比赛 曝光率
3. 为活动/比赛提供 电子门票
4. 为有综测加分的活动提供签到签退 加分电子数据
5. 激发南苑学子参与活动/比赛的热情， 打造南苑新社区

# 二、产品

#### 2.1 产品定位
南苑life+是为南苑师生、社团组织、商家所提供的综合性活动信息发布平台 APP，能够精准推广营销，打破信息壁垒。
1. 开发出新的信息获取渠道，帮助师生更容易获取校内的各种活动信息。
2. 帮助校内社团，商家提供信息发布平台。

#### 2.2产品理念
打造一个信息共享平台，不受信息壁垒所限制，为校内的师生提供多元化，时效性强且准确的信息，解决用户对于校内信息获取滞后的问题。

#### 2.3产品核心目标
将校园活动信息集中化，破除信息壁垒，规范校内各类信息的传播，营造一个传播正确信息的环境，成为校园官方信息发布渠道，及时规整校内资源信息，使南苑学子掌握上校内活动的一手信息。

![比赛](https://images.gitee.com/uploads/images/2021/0122/200111_a6a43771_4875306.png "比赛.png")

建立南苑活动社群，加强活动部门之间联系，增加观众与观众、观众与活动方之间的互动，激发南苑学子参加活动的热情，打造南苑新社区。
![圈子](https://images.gitee.com/uploads/images/2021/0122/200137_504eeff2_4875306.png "圈子.png")

#### 2.4产品亮点

|信息发布   |圈子   |综测加分   |电子门票   |
|---|---|---|---|
|解决校内活动信息的时效性不足的缺点，打破信息壁垒。|供学生参与活动或者话题的讨论，打造积极的活动社群，满足学生提供交流的需求。| 解决对综测讲座繁琐步骤，实现综测数据化。|提供电子门票，方便活动的管理和审核|



#### 2.5产品核心功能
 **2.5.1 发布/更新校园的活动/比赛** 

将信息的类型进行细分，然后在首页展示出不同类型的信息，并且提供活动搜索功能，使用户可以更加容易的获取最新的活动信息，或者搜索到自己想要了解到的信息。  
![文娱](https://images.gitee.com/uploads/images/2021/0122/200125_a228eb35_4875306.png "文娱.png")

 **2.5.2 报名参加活动**   
点击你感兴趣的活动，进入活动并填写基本信息，经过审核后即可参与该活动。  
![活动报名1](https://images.gitee.com/uploads/images/2021/0122/200353_21e1ecb3_4875306.png "活动报名1.png")
![活动报名2](https://images.gitee.com/uploads/images/2021/0122/200408_bff7d588_4875306.png "活动报名2.png")

 **2.5.3 综测数据电子化记录**   
将纸质版的综测电子化，避免综测加分条的遗失，并且能够让教职人员更加便利地统计学生的加分情况。  
![加分](https://images.gitee.com/uploads/images/2021/0122/200518_77880a57_4875306.png "加分.png")

 **2.5.4 创建圈子，打造活动社群**   
除了活动的内容，用户还可以在【圈子】功能中，参与活动或者话题的讨论，打造积极的活动社群，满足学生提供交流的需求。  
![圈子](https://images.gitee.com/uploads/images/2021/0122/200530_4f072220_4875306.png "圈子.png")

 **2.5.5 提供电子门票**   
提供电子门票，方便活动的管理和审核。  
![电子门票](https://images.gitee.com/uploads/images/2021/0122/200612_03a98caf_4875306.png "电子门票.png")

 **2.5.6 圈子内进行私聊群聊**   
提供私聊功能，使学生可以在平台更加方便，快速地和他人进行交流。  
![私聊](https://images.gitee.com/uploads/images/2021/0122/200709_256a63fc_4875306.png "私聊.png")

# 三、 市场概述
（1） **互联网平台服务收入增长平稳** ，1－11月，互联网平台服务企业实现业务收入3856亿元， 同比增长14.2%*，增速较1－10月回升0.4个百分点，占互联网业务收入比为33.6%。其中，以在线教育服务为主的企业增长再次提速；网络销售平台企业业务收入增幅明显提升；以提供生活服务平台服务为主的企业业务收入增速略有回落；以提供生产制造和生产物流平台服务为主的企业收入增速回落。 来源：中国人民共和国中央政府《2020年1－11月互联网和相关服务业运行情况》

（2） **2019年中国信息服务业务整体快速增长** ，收入达7879亿元。 **生活服务、网络销售服务规模不断扩大** 。2019年，互联网平台服务企业(以提供生产服务平台、生活服务平台、科技创新平台、公共服务平台等为主)实现业务收入3193亿元，同比增长24.9%，增速高于互联网业务收入3.5个百分点，占比达26.5%。

#### 3.1 市场问题

南苑life+是一个以促进南苑学生更为方便快捷的得到校园活动信息为目的的平台，此平台面向南苑全校师生以及校内商家，致力于为全校师生打造一个智能化、信息化校园。 目前定位是为中大南方学子提供一个校园活动信息发布平台。

当前校内信息发布途径存在的痛点，从 发布者、参与者 两个角度进行分析。

对于 发布者 有几个痛点问题：
- 宣传/发布途径过少，活动宣传范围较小
- 朋友/朋友圈进行宣传，其得知且参与者都为已认识的人，难以扩大活动的受众人群。
- 统计参与活动人数较为麻烦
- 提供综测加分的活动讲座需要纸质签到签退，不易保存手续麻烦

对于痛点1、2，目前市场是只有优化方案，即用“南苑万万”“果断点”等有较多人得知的渠道进行宣传，而痛点4目前是还没得到解决的。

对于 **参与者** 而言，需要关注多个公众号去获取活动信息，而在此前提下还是会遗漏活动信息，导致在许多时候会错过活动。
![第二题](https://images.gitee.com/uploads/images/2021/0122/143020_ccf1ede6_4875306.png "第二题.png")
![第三题](https://images.gitee.com/uploads/images/2021/0122/143029_782ff242_4875306.png "第三题.png")

####  **3.1.1 市场机会**  
根据以上的市场问题，我们提出了制作“南苑life+活动信息发布平台”小程序，将南苑所有活动汇集于小程序中。对此发布者可以在此平台发布活动，用户也可在此 查看不同类型的活动 。而院方也可在此平台上发布有关加综测的讲座，方便进行综测统计。

####  **3.2 市场细分** 
南苑life+除了有 **活动信息整合发布的功能之外** ，还有 **活动报名、电子门票管理、承接活动** 等功能。从市场趋势就可以看出许多平台也是在致力于多维发展。

1. 活动行最初就是一个活动发布平台，随着平台的发展壮大，增加了报名活动，活动类型多样，包括创业分享、线下沙龙、互联网会议、音乐会、亲子出游等。使平台的功能越来越多样化。
2. 周末去哪儿，这是一个专注于为用户提供周末休闲活动的app，与活动行不同的是，他专注于“周末活动”，并且有评论功能，给用户以参考，也可以给活动的主办方改进自身的建议。
3. 秀动--一个独立音乐小型演出（Livehouse）的购票平台，最初只是售卖一些小众rapper的live演出，后来与音乐人、livehouse场地主合作，包揽了承办、宣传、售卖门票。不仅时小众亚文化走向大众视野，还壮大了自身。

####  **3.3 市场壁垒**   

 **3.3.1 资金壁垒**     
平台在起步时，需要大额广告费将我们的信息平台推广出去，让更多的校内学生知道了解并使用我们的平台。如拼多多的邀请新用户活动。

 **3.3.2 技术壁垒**   
如小程序的搭建，信息爬取技术，这些都需要网络技术方面的人才。

 **3.3.3 垄断壁垒**   
学校的官方组织负责校内各项活动，我们能否取得其他组织的授权，将信息搬运到我们的平台上，是需要征得他们的同意的。
# 四、 需求分析

####  **4.1用户需求分析**   

 **4.1.1主要目标用户** 
- 南院的普通学生群体
- 南院的普通老师群体
- 社团部门

 **4.1.2用户痛点** 

1. 平时获取校内活动信息的渠道单一且片面，不能及时地去参加自己感兴趣的活动。
2. 只能通过社团公众号或是路边摆摊进行宣传，活动曝光率较小。
3. 纸质门票比较不环保而且会增加验票人员的压力
4. 综测算分复杂且麻烦，容易造成数据错漏

 **4.1.3用户需求** 

1. 师生了解详细校内活动信息并参加。
1. 活动主办方能够有效浏览收集到的报名信息。
1. 增加活动线上宣传热度。
1. 将纸质版综测加分凭条进行数据化处理，方便院系统计。
1. 进入南苑活动社群，结识新的朋友，丰富课余生活。

 **4.2用户类型细分** 

|类型|  |
|---|---|
| 参加活动型  |  南院的普通师生群体 |
|举办活动型 | 校内商家、活动主办方、社团部门、校外社群组织、外包活动承包商|
|学校官方型 |学校官方信息发布、综测统计部门


 **4.3 用户场景**   
![用户画像 (1)](https://images.gitee.com/uploads/images/2021/0122/152912_3fc951ce_4875306.jpeg "用户画像 (1).jpg")

![用户画像 (2)](https://images.gitee.com/uploads/images/2021/0122/152920_6df98a6d_4875306.jpeg "用户画像 (2).jpg")
# 五、 竞品分析

####  **5.1 竞品分析对象** 
|产品类型|竞品对象|   |
|---|---|---|
|直接竞品：| 活动行  | 活动行是全球最大的中文活动平台，主要以活动报名和售票为主，专为活动组织方 提供 从发布、推广、报名、收款、电子票务、二维码签到以及后期的名单管理等 一站式活动解決方案 。|
|间接竞品：| 果断点  |果断点是一款集订餐、订水、生活圈等功能为一体的 校内生活小程序 ，主要以订餐和订水为主，是校园师生们点外卖的平台之一以及订水的唯一途径，其生活圈功能是校内信息发布例如商家优惠、活动召集、二手闲置等。|




####  **5.2 商业模式** 
|产品|   |
|---|---|
|活动行|建立活动主办方与用户的O2O平台(online to offline)，通过门票付费分成、平台促销系统、广告服务等形式获取收入。   |
|果断点   | 以外卖、订水付费分成的形式获取部分收入，其他部分收入来源于广告的宣传。  |





####  **5.3 竞品目标用户** 
| 产品  | 用户  |   |
|---|---|---|
|   | 活动主办方  | 参与者  |
| 活动行  |  想要 **快速发布** 以及在线管理活动，但自身没有固定的发布渠道以及宣传渠道 建立自己的购票渠道所需的 成本太高 需要利用 电子票务 进行统计 需要对活动进行科学管理与 精准营销的推广  | 对于某个类型的活动感兴趣但无法找到适合的活动 想要 **寻找** 一同参与活动、共同交流还可以进行分享， **志同道合的朋友** |
|   | 商家  | 学生  |
|果断点|需要宣传渠道，利用一个有一定热度和影响力的平台进行品牌的宣传、曝光度的上涨，带来更多的知名度以及利益。|利用校内平台进行基本的校内生活，学生可进行生活订水以及校内饭堂的外卖订餐，还有发布信息的诉求，在平台上发布信息如闲置、拼车等。|

####  **5.4 竞品运营、推广、营销策略** 

 **活动行** 

| 对象  |   |
|---|---|
| 对于 活动主动方 |   |
|1.  **在线管理活动及电子票务功能**   |活动行提供一站式活动管理解决方案，从组织策划、收集信息、名单管理、营销推广，再到线上支付及活动分析对网站功能进行整合；并针对中小型有演绎、赛事的组织单位，提供全套的电子票务系统。使活动主办方只需登陆活动行网站或下载APP，就能有效地浏览报名信息、用于会场验票，这样就能省去一部分会务人员，省钱省力。|
|2.  **对活动进行科学管b理与精准营销** |活动行的报名表设置、报名后台信息等功能，由活动主办方自主管理。一旦有人报名，系统会自动将其信息同步更新至主办方在他们的后台，方便浏览报名进展的折线图、报名人信息及客群特质，协助主办方更精准地把握营销节奏、更新推广方案，有针对性地对目标客群做推广。活动行网站开放了报名表的嵌入代码、二维码营销工具和社交平台的分享功能，方便活动主办方有效利用社交平台进行推广。|
|对于参与者|   |
|1.  **活动推荐精确**| 帮助用户精确查找感兴趣的同城活动，活动的推荐符合用户的个人喜好。  |
|2.  **重视社群的建立** |聚集线下体验用户，在活动详情页面，只要报名即可加入活动圈，群聊，支持拍照和发图片，在我的主页栏目，还可以添加同伴和私聊。用户可以与共同参加此活动的人一起沟通、交流意见，增强用户社交体验。|





 **果断点**   
|对象   |   |
|---|---|
|对于商家   | 品牌与活动的宣传 商家与果断点进行合作，使果断点官方进行自身品牌的宣传，除此之外还可通过生活圈发布帖子进行广告宣传。  |
|对于学生   |校内生活的固定平台 在果断点上进行生活订水是校内唯一的订水渠道，校内饭堂的外卖订餐也是唯一的渠道，此外学生可通过生活圈发布帖子，例如二手物品的闲置、拼车、社团活动的宣传等等。   |

####  **5.5 竞品技术分析** 

 **活动行** 
|所需技术|   |
|---|---|
|1.  **小程序开发和运维**      | 小程序开发需要掌握WXML 和 WXSS 以及基于JavaScript的逻辑层框架，提供视图层描述语言。运维人员需要考虑比如数据库运维，文件存储、内容加速、网络防护、容器服务、负载均衡、安全加固等等一系列的问题。  |
|2.  **网站制作与运营**    |网站则涉及到网站界面设计，实现多终端自适应布局，保障网站访问迅速及确保稳定运行，以及后台管理的SEO。   |

 **果断点** 

果断点只有小程序，所需的技术只有小程序开发和运维

####  **5.6 竞品市场份额** 

|产品   | 市场状况  |
|---|---|
|活动行   | 活动行近日完成数千万A+轮融资，估值超过三亿。活动行于2014年获得软银赛富(SAIF)、高通、DCM投资，深耕北上广深一线城市数年，目前已成为国内最大的活动管理及推广的SaaS平台。活动通从工具开始，起初包括活动报名、收款、出票验证等功能，后又做到了活动整包，现在依旧在台湾市场占据较大份额。并在近日与海外商业模式的协助，助推活动行海外版“活动通”抢占日本市场。  |
|果断点   | 果断点在中大南方以及多所高校等目标市场占有一定的份额，特别是外卖服务及桶装水服务。 但生活圈中关于信息发布召集贴子发布并没有很多人关注。|


# 六、 结论

####  **6.1 未来市场预判** 

校内师生方面：

1. 追求更高质量的活动/比赛推荐
2. 精准的兴趣活动推荐
3. 希望信息更加集中

活动主办方/商家方面：

1. 策划组织活动的需求
2. 根据宣传效果给予相应宣传费用的承诺协议
3. 从前期筹备、采购、预定场地物料、执行等都可以在本平台上完成


####  **6.2 风险、困难** 

1. 校内相似的信息分享公众号、群众多，竞争压力大，这些公众号存活时间长、粉丝量大，我们平台一开始用户数量不多，难以吸引用户，初期推广困难
2. 在校内举办活动需获得学校的同意，学校的意见对本平台有比较直接的影响
3. 举办的活动质量上能否吸引学生？活动本质与活动的现场是否一致？需严防虚假宣传
5. 对校内商家来说，有可能存在出钱出力的情况下并没有得到任何回报，付出与回报不成正比，有做广告的嫌疑，消费者可能不会去信任

####  **6.3 市场潜力、机会** 

1. 目前，校园活动传播的平台分散，时效性低，集合活动是一个有力的潜在市场
2. 学生需要更多优质有趣的活动来丰富校园生活
3. 发布纸质门票浪费资源
4. 对于师生用户来说，信息集中节省了用户的时间、打破信息壁垒、丰富优质的活动能很好地充实大学生活；对于活动主办方/社团部门来说，能够更好的更大范围的宣传活动、门票电子化节约门票成本；对于赞助方的商家来说，让它的品牌得到曝光度 带来更多的知名度以及利益 提供宣传渠道；对于学校来说，能够举办更多优质活动可以吸引更多的学生，提高学校知名度和吸引力。